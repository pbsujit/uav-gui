from flask import Flask, render_template, request, send_from_directory
import time
import math
import json
from pymavlink import mavutil
# Import DroneKit-Python
from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
import dronekit_sitl

def get_location_metres(original_location, dNorth, dEast):
    
    #Returns a LocationGlobal object containing the latitude/longitude `dNorth` and `dEast` metres from the 
    #specified `original_location`. The returned Location has the same `alt` value
    #as `original_location`.

    #The function is useful when you want to move the vehicle around specifying locations relative to 
    #the current vehicle position.
    #The algorithm is relatively accurate over small distances (10m within 1km) except close to the poles.
    #For more information see:
    #http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    
    earth_radius=6378137.0 #Radius of "spherical" earth
    #Coordinate offsets in radians
    dLat = dNorth/earth_radius
    dLon = dEast/(earth_radius*math.cos(math.pi*original_location.lat/180))

    #New position in decimal degrees
    newlat = original_location.lat + (dLat * 180/math.pi)
    newlon = original_location.lon + (dLon * 180/math.pi)
    return LocationGlobal(newlat, newlon,original_location.alt)


def get_distance_metres(aLocation1, aLocation2):
    
    #Returns the ground distance in metres between two LocationGlobal objects.

    #This method is an approximation, and will not be accurate over large distances and close to the 
    #earth's poles. It comes from the ArduPilot test code: 
    #https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    return math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5



def distance_to_current_waypoint(vehicle):
    
    #Gets distance in metres to the current waypoint. 
    #It returns None for the first waypoint (Home location).
    
    nextwaypoint = vehicle.commands.next
    if nextwaypoint==0:
        return None
    missionitem=vehicle.commands[nextwaypoint-1] #commands are zero indexed
    lat = missionitem.x
    lon = missionitem.y
    alt = missionitem.z
    targetWaypointLocation = LocationGlobalRelative(lat,lon,alt)
    distancetopoint = get_distance_metres(vehicle.location.global_frame, targetWaypointLocation)
    return distancetopoint

def arm(vehicle):

    #Arms vehicle and fly to aTargetAltitude.


    print "Basic pre-arm checks"
    # Don't let the user try to arm until autopilot is ready
    while not vehicle.is_armable:
        print " Waiting for vehicle to initialise..."
        time.sleep(1)

        
    print "Arming motors"
    # Copter should arm in GUIDED mode
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    while not vehicle.armed:      
        print " Waiting for arming..."
        time.sleep(1)


def starting_mission(vehicle,totalWaypoints):

    print "Starting mission"
    # Reset mission set to first (0) waypoint
    vehicle.commands.next=0

    # Set mode to AUTO to start mission
    vehicle.mode = VehicleMode("AUTO")

    while True:

        nextwaypoint=vehicle.commands.next

        print 'Distance to waypoint (%s): %s' % (nextwaypoint, distance_to_current_waypoint(vehicle))
      
        if nextwaypoint==totalWaypoints: #Skip to next waypoint
            print 'Skipping to Waypoint 5 when reach waypoint 3'
            vehicle.commands.next = totalWaypoints+2
        if nextwaypoint==totalWaypoints+2: #Dummy waypoint - as soon as we reach waypoint 4 this is true and we exit.
            print "Exit 'standard' mission when start heading to final waypoint (5)"
            break;
        time.sleep(1)


    print 'Return to launch'
    vehicle.mode = VehicleMode("RTL")

    #Close vehicle object before exiting script
    print "Close vehicle object"
    vehicle.close()

    # Shut down simulator if it was started.
    if sitl is not None:
        sitl.stop()


app = Flask(__name__)
 
@app.route("/")
def hello():
    return render_template("/bing_map.html")

@app.route("/get-way", methods = ["POST"])
def get_way():
    	asd = request.json  #This is asd example - ["[MapLocation (28.739754528963623, 77.11734008789058)]","[MapLocation (28.69880646188139, 77.1454925537109)]"]

    	new = str(asd)
        
        sitl = dronekit_sitl.start_default()
        connection_string = sitl.connection_string()

        # Connect to the Vehicle.
        print("Connecting to vehicle on: %s" % (connection_string,))
        vehicle = connect(connection_string, wait_ready=True)
        print("clearing previous commands")

        while not vehicle.home_location:
            cmds = vehicle.commands
            cmds.download()
            cmds.wait_ready()
            if not vehicle.home_location:
                print " Waiting for home location ..."
        # We have a home location, so print it!        
        print "\n Home location: %s" % vehicle.home_location
        vehicle.home_location = LocationGlobal(28.5445152224, 77.2719385965, 222)


    	### Make a function which extracts the coordinates from asd and uploads it to mission planner. Call that function here instead of "print asd" below
    	### Make the entire functionality outside and just call the function here with the coordinates from 'new'.

        #Add MAV_CMD_NAV_TAKEOFF command. This is ignored if the vehicle is already in the air.
        cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_TAKEOFF, 0, 0, 0, 0, 0, 0, 0, 0, 10))

    	new = new.split('"')
        totalWaypoints = len(new)-1
    	
    	for i in range(1,len(new),2):
    	    a = new[i]
    	    c = a.find(',')
    	    lat = float(a[14:c])
    	    lon = float(a[c+2:-2])
            print lat,lon
            cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, lat, lon, 11));


        cmds.upload()

        arm(vehicle)
        
        starting_mission(vehicle,totalWaypoints)

    	return asd





if __name__ == "__main__":

    app.run("127.0.0.1",5003)
    
    

    


    




