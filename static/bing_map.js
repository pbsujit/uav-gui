        var layer, map, searchManager, finalText;
        var final_way = [];
        var count  = 0;
        function GetMap()
        {
            map = new Microsoft.Maps.Map('#myMap', {
                credentials: 'Ai4Ow1XYPrRUtKTtKk3qHLtm3fWSFgywNyBuqzXdNIW4nvnt5b6RJ-UuYm7OQ0hv'
            });
            Microsoft.Maps.Events.addHandler(map, 'click',getLatlng ); 
           layer = new Microsoft.Maps.Layer();
           map.layers.insert(layer);
            Microsoft.Maps.Events.addHandler(layer, 'rightclick', function (e) {
                var shape = e.primitive;
                if (shape instanceof Microsoft.Maps.Pushpin) {
                    //alert('Pushpin right clicked');
                    layer.remove(shape);
                    var all_pins = layer.getPrimitives();
                    var num = 0;
                    for (var i = 0;i<all_pins.length;i++){
                        var shape = all_pins[i];
                        if (shape instanceof Microsoft.Maps.Pushpin){
                            num++;
                            shape.setOptions({text:String(num)});
                        }
                    }
                    //map.layers.insert(layer);
                } else {
                    //alert('Polyline or Polygon right clicked');
                }
            });
        }
        function ClearWay(){
            layer.clear();
        }
        function getLatlng(e) { 
            if (e.targetType == "map") {
               var point = new Microsoft.Maps.Point(e.getX(), e.getY());
               var locTemp = e.target.tryPixelToLocation(point);
               var location = new Microsoft.Maps.Location(locTemp.latitude, locTemp.longitude);
             //alert(locTemp.latitude+"&"+locTemp.longitude);

               var pin = new Microsoft.Maps.Pushpin(location, {'draggable': true});
                 layer.add(pin);
                 var all_pins = layer.getPrimitives();
                 var num = 0;
                 for (var i = 0;i<all_pins.length;i++){
                    var shape = all_pins[i];
                    if (shape instanceof Microsoft.Maps.Pushpin){
                        num++;
                        shape.setOptions({text:String(num)});
                    }
                 }
            }              
           }
           function Search() {
            if (!searchManager) {
                //Create an instance of the search manager and perform the search.
                Microsoft.Maps.loadModule('Microsoft.Maps.Search', function () {
                    searchManager = new Microsoft.Maps.Search.SearchManager(map);
                    Search()
                });
            } else {
                //Remove any previous results from the map.
                map.entities.clear();

                //Get the users query and geocode it.
                var query = document.getElementById('searchTbx').value;
                geocodeQuery(query);
            }
        }

        function geocodeQuery(query) {
            var searchRequest = {
                where: query,
                callback: function (r) {
                    if (r && r.results && r.results.length > 0) {
                        var pin, pins = [], locs = [], output = 'Results:<br/>';

                        for (var i = 0; i < r.results.length; i++) {
                            //Create a pushpin for each result. 
                            pin = new Microsoft.Maps.Pushpin(r.results[i].location, {
                                text: i + ''
                            });
                            pins.push(pin);
                            locs.push(r.results[i].location);

                            output += i + ') ' + r.results[i].name + '<br/>';
                        }

                        //Add the pins to the map
                        //map.entities.push(pins);

                        //Display list of results
                        //document.getElementById('output').innerHTML = output;

                        //Determine a bounding box to best view the results.
                        var bounds;

                        if (r.results.length == 1) {
                            bounds = r.results[0].bestView;
                        } else {
                            //Use the locations from the results to calculate a bounding box.
                            bounds = Microsoft.Maps.LocationRect.fromLocations(locs);
                        }

                        map.setView({ bounds: bounds });
                    }
                },
                errorCallback: function (e) {
                    //If there is an error, alert the user about it.
                    alert("No results found.");
                }
            };

            //Make the geocode request.
            searchManager.geocode(searchRequest);
        }
        function genWay(){
            var all_pins = layer.getPrimitives();
            final_way.length = 0;
            finalText = ""
            var num = 0;
            for (var i = 0; i<all_pins.length; i++){
                curr = all_pins[i];
                if (curr instanceof Microsoft.Maps.Pushpin){
                    final_way[num] = curr.getLocation().toString();
                    num++;
                }
            }
            finalText = JSON.stringify(final_way);
            console.log(finalText);
        }
        $(document).ready( function() {
        $('#submit').click(function() {
           var formdata = finalText;
           $.ajax({
                 type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(formdata),
                dataType: 'json',
                url: 'http://127.0.0.1:5003/get-way',
                success: function (e) {
                    console.log(e);
                    //window.location = "http://127.0.0.1:5000/";
                },
                error: function(error) {
                console.log(error);
            }
            });
        });
  });